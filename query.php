<?php

require 'vendor/autoload.php';

use Jaeger\GHttp;
use QL\QueryList;

$rules = [
    'url' => ['#tit-A-content > ul:nth-child(1) > li > a', 'href']
];
$url = 'http://www.gdstc.gov.cn';
$html = GHttp::get($url);

$data = QueryList::html($html)->rules($rules)->query()->getData();
$data = $data->all();
var_dump($data);